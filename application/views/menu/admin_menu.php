<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url();?>assets/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Administrator</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?php if($this->uri->segment(1)=="admin" && $this->uri->segment(2)=="dashboard"){echo "active";}else{echo "";}?>"><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
      </li>
      <li class="<?php if($this->uri->segment(1)=="admin" && $this->uri->segment(2)=="informasi"){echo "treeview active";}else{echo "treeview";}?>">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>Master Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($this->uri->segment(2)=="siswa"){echo "active";}else{echo "";}?>"><a href="<?php echo site_url();?>admin/siswa"><i class="fa fa-circle-o"></i> Data Siswa</a></li>
          <li class="<?php if($this->uri->segment(2)=="jadwal"){echo "active";}else{echo "";}?>"><a href="<?php echo site_url();?>admin/jadwal"><i class="fa fa-circle-o"></i> Data Jadwal</a></li>
        </ul>
      </li>
      
      
      <li class="<?php if($this->uri->segment(1)=="admin" && $this->uri->segment(2)=="informasi"){echo "treeview active";}else{echo "treeview";}?>">
        <a href="#">
          <i class="fa fa-book"></i>
          <span>Laporan</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($this->uri->segment(3)=="laporan"){ echo "active"; }else{ echo ""; } ?>"><a href="<?php echo site_url();?>admin/laporan"><i class="fa fa-circle-o"></i> Laporan</a></li>
        </ul>
      </li>
      


      <li class="<?php if($this->uri->segment(1)=="profile" || $this->uri->segment(2)=="config"){ echo "treeview active"; }else{ echo "treeview"; } ?>">
        <a href="#">
          <i class="fa fa-gears"></i> 
          <span>Pengaturan</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($this->uri->segment(1)=="profile"){ echo "active"; }else{ echo ""; } ?>"><a href="<?php echo base_url(); ?>admin/profile"><i class="fa fa-user"></i> Ubah Profile</a></li>
          <li class="<?php if($this->uri->segment(1)=="profile"){ echo "active"; }else{ echo ""; } ?>"><a href="<?php echo base_url(); ?>admin/profile"><i class="fa fa-user"></i> Tambah Admin</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>