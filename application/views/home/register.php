<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PPDB PIN | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="<?php echo base_url();?>"><b>Aplikasi Register Pin PPDB</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register Pengambilan PIN PPDB</p>

    <form action="<?php echo base_url()."do_reg"; ?>" method="post">

      <div class="form-group has-feedback">
        <input type="hidden" class="form-control" name="no_reg" id="no_reg" value="<?php echo $kode; ?>" readonly="readonly"> 
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="nisn" id="nisn" placeholder="NISN" onkeyup="cek_nisn()" maxlength="10" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        <p id="pesan_nisn"></p>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="hp" id="hp" placeholder="No Hp" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" id="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
      <?php if ($this->session->flashdata('error')): ?>

            <?php echo $this->session->flashdata('error'); ?>

          <?php endif; ?>

          <?php if ($this->session->flashdata('success')): ?>
            

            <?php echo $this->session->flashdata('success'); ?>

          <?php endif; ?>
    </form>

    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="<?php echo base_url()."jadwal"; ?>" target="_blank" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-list"></i> Cek Jadwal Pengambilan PIN</a>
    </div>

  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/AdminLTE/plugins/iCheck/icheck.min.js"></script>

<script type='text/javascript'>

var error = 2; 

function cek_nisn(){
  $("#pesan_nisn").hide();

  var nisn = $("#nisn").val();

  if(nisn != ""){
    $.ajax({
      url: "<?php echo site_url('home/cek_status_nisn');?>", 
      data: 'nisn='+nisn,
      type: "POST",
      success: function(msg){
        if(msg==2){
          $("#pesan_nisn").css("color","#59c113");
          $("#nisn").css("border-color","#59c113");
          $("#pesan_nisn").html(" NISN belum terdaftar");
          $("#submit").attr('disabled',false);

          error = 2;
        }else{
          $("#pesan_nisn").css("color","#fc5d32");
          $("#nisn").css("border-color","#fc5d32");
          $("#pesan_nisn").html("NISN Sudah terdaftar");
          $("#submit").attr('disabled','');
          error = 1;
        }

        $("#pesan_nisn").fadeIn(1000);
      }
    });
  }       
  
}

</script> 

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
