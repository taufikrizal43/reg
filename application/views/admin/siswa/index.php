<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
   <!--modal  pegawai -->

   <div class="row">
    <div class="col-xs-12">
      <?php
      $error = $this->session->flashdata('error');
      if($error!=""){ ?>
       <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><center><strong>Error ! </strong> <?=$error;?>
       <center></div>
       <?php } ?>
       <?php
       $info = $this->session->flashdata('Info');
       if($info!=""){ ?>
         <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><center><strong>Sukses ! </strong> <?=$info;?>
         <center></div>
         <?php } ?>
         <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Siswa</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No ID</th>
                  <th>Nisn</th>
                  <th>Nama Lengkap</th>
                  <th>Email</th>
                  <th>No Hp</th>
                  <th>Nomor Registrasi</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($data->result() as $data): ?>
                  <tr>
                    <td><?php echo $data->id_req; ?></td>
                    <td><?php echo $data->nisn; ?></td>
                    <td><?php echo $data->name; ?></td>
                    <td><?php echo $data->email; ?></td>
                    <td><?php echo $data->hp; ?></td>
                    <td><?php echo $data->no_reg; ?></td>
                    <td><div class="action-buttons">
                      <a class="red" href="<?php echo base_url();?>admin/siswa/delete/<?php echo $data->id_req;?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin akan menghapus data ini?');">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                      </a>
                    </div></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
