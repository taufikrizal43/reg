<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Horizontal Form -->
      <div class="col-md-12">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Edit Jadwal</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/jadwal/dotambah" enctype="multipart/form-data">
            <div class="box-body">
             <input type="hidden" name="id_jadwal" id="id_jadwal" value="<?php echo $id_jadwal; ?>">
             <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Range NO Regisreasi</label>

              <div class="col-sm-10">
                <input type="text" name="no_reg" id="no_reg" placeholder="Range NO Regisreasi" class="form-control" value="<?php echo $no_reg; ?>" required>
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Isi</label>

              <div class="col-sm-10">
               <textarea id="editor1" name="keterangan" rows="10" cols="80">
                 <?php echo $keterangan; ?>
               </textarea>
             </div>

           </div>


        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <a href="<?php echo base_url();?>admin/jadwal" class="btn btn-primary"><i class="fa fa-close"></i>Tutup</a>
          <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          
        </form>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
  <!-- /.box -->
</div>
</div>
</section>

</div>