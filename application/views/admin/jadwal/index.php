<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Main content -->
  <section class="content">
    <!-- /.modal -->
    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Jadwal Pengambilan PIN</h4>
              <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>admin/jadwal/dotambah" enctype="multipart/form-data">
              </div>
              <div class="modal-body">
                <input type="hidden" name="id_jadwal" id="id_jadwal" value="<?php echo $id_jadwal; ?>">

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Range NO Regisreasi</label>

                  <div class="col-sm-10">
                    <input type="text" name="no_reg" id="no_reg" placeholder="Range NO Regisreasi" class="form-control" value="<?php echo $no_reg; ?>" required>
                  </div>
                  <br>
                  <span> contoh : PPDB-2020-0001 sd PPDB-2020-0020</span>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Keterangan</label>
                  <div class="col-sm-10">
                    <textarea id="editor1" name="keterangan" id="keterangan" rows="10" cols="80">
                      <?php echo $keterangan; ?>
                    </textarea>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit"  class="btn btn-primary">Simpan </button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- /.modal -->
      <div class="row">
        <div class="col-xs-12">
          <?php
          $error = $this->session->flashdata('error');
          if($error!=""){ ?>
           <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><center><strong>Error ! </strong> <?=$error;?>
           <center></div>
           <?php } ?>
           <?php
           $info = $this->session->flashdata('Info');
           if($info!=""){ ?>
             <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><center><strong>Sukses ! </strong> <?=$info;?>
             <center></div>
             <?php } ?>
             <div class="box">
              <div class="box-header">
                <h3 class="box-title">Jadwal PPDB </h3>
                <div class="pull-right box-tools">
                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">
                    <i class="fa fa-plus"> </i>  Tambah  Data</button>
                  </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body table-responsive">
                  <table id="example1" class="table table-bordered table-striped" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nomor Registrasi</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no=1; foreach($data->result() as $data): ?>
                        <tr>
                          <td><?php echo $no++ ?></td>
                          <td><?php echo $data->no_reg; ?></td>
                          <td><?php echo $data->keterangan; ?></td>
                          <td><div class="action-buttons">
                            
                            <a class="green" href="<?php echo base_url()?>admin/jadwal/edit/<?php echo $data->id_jadwal; ?>">
                              <i class="ace-icon fa fa-pencil bigger-130"></i>
                            </a>
                            <a class="red" href="<?php echo base_url();?>admin/jadwal/delete/<?php echo $data->id_jadwal;?>" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin akan menghapus data ini?');">
                              <i class="ace-icon fa fa-trash-o bigger-130"></i>
                            </a>
                          </div></td>
                          <td>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
<!-- /.content-wrapper -->