<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Laporan
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">laporan</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-6 col-xs-12">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>Semua Laporan</h3>

            <p>Semua Data</p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <br>
          <a href="<?php echo base_url();?>admin/laporan/cetak" class="small-box-footer">
            Download Laporan <i class="fa fa-cloud-download"></i>
          </a>
        </div>
      </div>
      <!-- ./col -->
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->