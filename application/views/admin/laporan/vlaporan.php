<!DOCTYPE html>
<html>
<head>
  <title><?=$title?></title>
  <style>
  table{
    border-collapse: collapse;
    width: 100%;
    margin: 0 auto;
  }
  table th{
    border:1px solid #000;
    padding: 3px;
    font-weight: bold;
    text-align: center;
  }
  table td{
    border:1px solid #000;
    padding: 3px;
    vertical-align: top;
  }
</style>
</head>
<body>
  <p style="text-align: center" linespacing="1">
    <b>LAPORAN DATA PPDB 2020</b>
    <br>
    <b>SMKN 1 Nglegok </b>
    <br>
    Jl. Raya Penataran No.1, Nglegok 1, Nglegok, Blitar, Jawa Timur 66181
    <br>
    Telp. (0342) 561355
    <hr>
  </p>

  <table>
    <tr style="background: #3d82d0;">
      <th style="width: 3%">No</th>
      <th style="width: 10%">ID Registrasi</th>
      <th style="width: 7%">NISN</th>
      <th style="width: 10%">Nama</th>
      <th style="width: 10%">Email</th>
      <th style="width: 10%">Nmor HP</th>
      <th style="width: 10%">Nomor Registrasi</th>
    </tr>
    <?php $no=1; foreach($data as $row){
      ?>
      <tr>
        <td><?php echo $no++;?></td>
        <td><?php echo $row->id_req;?></td>
        <td><?php echo $row->nisn;?></td>
        <td><?php echo $row->name;?></td>
        <td><?php echo $row->email;?></td>
        <td><?php echo $row->hp;?></td>
        <td><?php echo $row->no_reg;?></td>
      </tr>
      <?php }?>
    </table>
  </body>
  </html>