<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','html'));
		$this->load->model('model_login');
	}

	public function index()
	{

		$this->load->view('home/login');
	}

	public function dologin()
	{
		$username  = $this->input->post('username');
		$password  = md5($this->input->post('password'));

		$cek_admin = $this->model_login->ceknum($username,$password);
		$cek_member = $this->model_login->ceknum($email,$password);

		if($cek_admin->num_rows() == 1)
		{
			foreach($cek_admin->result() as $row)
			{
				$pass_auth = $row->password;

				if($password == $pass_auth)
				{
					
					$row_data['id'] 	   	= $row->id;
					$row_data['username'] 	= $row->username;
					
					$this->session->set_userdata($row_data);
					redirect('admin/dashboard');
				}
			else
			{
						//$data['error']='Wrong Password!';
				$this->session->set_flashdata('error','Username / Password Salah.');
				redirect('login');
			}

		}
	}
	elseif($cek_member->num_rows() == 1)
	{
		foreach($cek_member->result() as $row)
		{
			$pass_auth = $row->password;

			if($password == $pass_auth)
			{
				
				$row_data['id']   = $row->id;
				$row_data['email'] = $row->email;
				
				$this->session->set_userdata($row_data);
				redirect('alumni/dashboard');
			}
		else
		{
						//$data['error']='Wrong Password!';
			$this->session->set_flashdata('error','Email/ Password Salah.');
			redirect('login');
		}

	}
	}
	else
	{
				//$data['error']='Wrong Username!';
		$this->session->set_flashdata('error','Username / Password Salah.');
		redirect('login');
	}
	
}

function logout(){
	$this->session->sess_destroy();
	redirect(base_url('login'));
}

}
