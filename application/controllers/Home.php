<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','html'));
		$this->load->model('model_reg');
		$this->load->model('model_jadwal');
	}

	public function index()
	{
		$data['kode'] = $this->model_reg->buat_kode();
		$this->load->view('home/register',$data);
	}

	public function login(){

		$this->load->view('home/login');
	}

	public function register(){

		$data['kode'] = $this->model_reg->buat_kode();
		$this->load->view('home/register',$data);
	}

	public function do_reg(){

		$email	= $this->input->post('email');
		$no_reg = $this->input->post('no_reg');


		$data= array(
			'nisn' => $this->input->post('nisn'),
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'hp' => $this->input->post('hp'),
			'active' => 0,
			'no_reg'=>$this->input->post('no_reg')
		);

		// $data['nisn'] 			= $this->input->post('nisn');
		// $data['name'] 			= $this->input->post('name');
		// $data['email'] 			= $this->input->post('email');
		// $data['hp'] 			= $this->input->post('hp');
		// $data['active'] 		= $this->input->post(0);
		// $data['no_reg'] 		= $this->input->post('no_reg');

		$this->model_reg->daftar_reg($data);

		$encrypted_id = md5($email);
		$this->load->library('email');
		$config = array();
		$config['charset'] ='utf-8';
		$config['useragent'] = 'Codeigniter';
    	$config['protocol']= "smtp";
    	$config['mailtype']= "html";
    	$config['smtp_host']= "ssl://smtp.gmail.com";
    	$config['smtp_port']= "465";
    	$config['smtp_timeout']= "400";
    	$config['smtp_user']= "tkjsmkn1nglegok@gmail.com"; // isi dengan email 
    	$config['smtp_pass']= "tkjhebat@321"; // isi dengan password 
    	$config['crlf']="\r\n"; 
    	$config['newline']="\r\n"; 
    	$config['wordwrap'] = TRUE;
    	$this->email->initialize($config);
    	//konfigurasi pengiriman
    	$this->email->from($config['smtp_user']);
    	$this->email->to($email);
    	$this->email->subject("Nomor Registratsi Pengambilan PIN PPDB SMKN 1 Nglegok");
    	$this->email->message("Terimakasih $email telah melakuan registrasi PIN PPDB SMKN 1 NGLEGOK Tahun 2020, <h1> Nomor Registratsi : $no_reg </h1> Untuk  info jadwal Pengambilan PIN  silahkan klik link dibawah ini<br><br>".
      site_url('jadwal'));
  
    if($this->email->send())
    {
        $this->session->set_flashdata('success','Pendaftaran Berhasil. Silahkan Cek email ya kak!!!!') ;
    }else{
        $this->session->set_flashdata('success','Tapi gagal mengirim verifikasi email huhuhuhu.');
    }
		//$this->session->set_flashdata('success','Pendaftaran Berhasil. Silahkan Login.');

		redirect('register');
		
	}

	public function cek_status_nisn(){
		$nisn = $this->input->post('nisn');
		
		$hasil_nisn = $this->model_reg->cek_nisn($nisn);
		
		if(count($hasil_nisn)!=0){ 
			echo "1"; 
		}else{
			echo "2";
		}
		
	}

	public function hasil_reg(){

		$this->load->view('home/hasil');
	}

	function get_nomor_reg(){

		$nisn = $this->input->post('nisn');

        $data = $this->model_reg->get_data_nisn($nisn);

        echo json_encode($data);
	}

	public function jadwal(){

		$data['data']			= $this->model_jadwal->getjadwal();
		
		$this->load->view('home/jadwal',$data);
		
	}
}
