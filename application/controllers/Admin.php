<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		admin_logged_in();
		$this->load->helper(array('form','url','html'));
		$this->load->library('pdf');
		$this->load->model('model_admin');
		$this->load->model('model_reg');
		$this->load->model('model_laporan');
	}

	public function index()
	{
		$data['main_content'] 	= 'admin/home';
		$data['title'] 			= 'Home';
		$data['jml_siswa']  	= $this->model_admin->count('total_siswa');
		$data['jml_user']  		= $this->model_admin->count('total_user');
		$data['user'] 			= $this->model_admin->getUser($this->session->userdata('id'));
		$this->load->view('admin_page',$data);
	}

	public function siswa()
	{
		
		$data['main_content'] 	= 'admin/siswa/index';
		$data['title'] 			= 'siswa';
		$data['user'] 			= $this->model_admin->getUser($this->session->userdata('id'));
		$data['data']			= $this->model_reg->getsiswa();
		$this->load->view('admin_page', $data);
	}

	public function delete_siswa()
	{

		$key = $this->uri->segment(4);
		
		$this->model_reg->getdelete_siswa($key);
		$this->session->set_flashdata('Info','Data berhasil di hapus');
		redirect('admin/siswa/index');
	}

	public function jadwal()
	{
		$data['main_content'] 	= 'admin/jadwal/index';
		$data['title'] 			= 'jadwal';
		$data['data']			= $this->model_admin->getjadwal($this->uri->segment(4));

		$data['id_jadwal'] 		= '';
		$data['no_reg'] 			= '';
		$data['keterangan'] 		= '';
		
		$this->load->view('admin_page', $data);

	}

	public function simpan_jadwal()
	{
		$data['id_jadwal'] 		= $this->input->post('id_jadwal');
		$data['no_reg'] 		= $this->input->post('no_reg');
		$data['keterangan'] 	= $this->input->post('keterangan');

		$key= $this->input->post('id_jadwal'); 
		
		$query = $this->model_admin->getdatajadwal($key);
		if($query->num_rows() > 0)
		{
			
			$this->model_admin->getupdate_jadwal($key,$data);
			$this->session->set_flashdata('Info','Data berhasil di update');
		}else{
			$this->model_admin->getinsert_jadwal($data);
			$this->session->set_flashdata('Info','Data berhasil di simpan');
		}
		
		redirect('admin/jadwal/index');
	}

	public function edit_jadwal()
	{
		$data['main_content'] 	= 'admin/jadwal/edit_jadwal';
		$data['title'] 			= 'Produk';
		$data['data']			= $this->model_admin->getjadwal($this->uri->segment(4));

		$key = $this->uri->segment(4);
		$query = $this->model_admin->getdatajadwal($key);
		if($query->num_rows()>0)
		{
			foreach ($query->result() as $row) 
			{
				$data['id_jadwal'] 		= $row->id_jadwal;
				$data['no_reg'] 		= $row->no_reg;
				$data['keterangan'] 	= $row->keterangan;
				
			}
		}
		else{
			$data['id_jadwal'] 		= '';
			$data['no_reg'] 		= '';
			$data['keterangan'] 	= '';
			

		}
		$this->load->view('admin_page', $data);
	}

	public function delete_jadwal()
	{

		$key = $this->uri->segment(4);
		
		$this->model_admin->getdelete_jadwal($key);
		$this->session->set_flashdata('Info','Data berhasil di hapus');
		redirect('admin/jadwal/index');
	}

	    public function laporan()
    {

    	$data['main_content'] 	= 'admin/laporan/laporan';
    	$data['title'] 			= 'Laporan';
    	$this->load->view('admin_page', $data);
    }

        public function cetakpdf(){
    	$data['title'] = 'Cetak PDF Semua Data PPDB 2020'; 
    	$data['data'] = $this->model_laporan->semua();

    	$this->load->view('admin/laporan/vlaporan', $data);

    	$paper_size  = 'legal'; 
    	$orientation = 'landscape'; 
    	$html = $this->output->get_output();

    	$html = preg_replace('/>\s+</', '><', $html);
    	$this->pdf->set_paper($paper_size, $orientation);
        //Convert to PDF
    	$this->pdf->load_html($html);
    	$this->pdf->render();
    	$this->pdf->stream("ppdb_2020_reg_pin.pdf", array('Attachment'=>0));
    }

	
}
