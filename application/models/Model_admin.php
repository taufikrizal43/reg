<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_admin extends CI_Model {

	function getUser($id){

		$this->db->where('id', $id);
       $query = $this->db->get('admin');
       return $query->row();
   }

   function count($by){
      switch ($by) {

        case 'total_siswa':
        $this->db->select("COUNT(*) as jml");
        $result = $this->db->get('tbl_req');
        $result = $result->row_array();
        return $result['jml'];
        break;

        case 'total_user':
        $this->db->select("COUNT(*) as jml");
        $result = $this->db->get('admin');
        $result = $result->row_array();
        return $result['jml'];
        break;

        case 'total_agenda':
        $this->db->select("COUNT(*) as jml");
        $result = $this->db->get('agenda');
        $result = $result->row_array();
        return $result['jml'];
        break;

        default:
        return 0;
        break;
        }
    }

    public function getdatajadwal($key)
    {
        $this->db->where('id_jadwal', $key);
        $hasil = $this->db->get('jadwal');
        return $hasil;
    }

        public function getjadwal(){
        $this->db->select('*');
        $this->db->from('jadwal');
        $this->db->order_by('id_jadwal','ASC');

        return $query=$this->db->get();
        if ($query->num_rows()>0) {
            return $query->result();
        }
    }

    public function getinsert_jadwal($data)
    {
        $this->db->insert('jadwal',$data);
    }

    public function getupdate_jadwal($key,$data)
    {
        $this->db->where('id_jadwal',$key);
        $this->db->update('jadwal',$data);
    }

    public function getdelete_jadwal($key)
    {
        $this->db->where('id_jadwal',$key)
        ->delete('jadwal');
    }

}