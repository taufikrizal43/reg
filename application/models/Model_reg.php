<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_reg extends CI_model {


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	

	public function daftar($key,$data)
	{
		$this->db->insert('tbl_req',$data);
	}

	public function daftar_reg($data)
	{
		$this->load->database();
		

		return $this->db->insert('tbl_req',$data);
	}


	
	function cek_nisn($nisn){
		$sql = "SELECT * from tbl_req where nisn='$nisn'";

		$hasil = $this->db->query($sql);
	//if($hasil->num_rows() >0){
		return $hasil->result();
	//}else{
		//return false;
	//}
	}

	public function buat_kode()   {

		$this->db->select('RIGHT(tbl_req.id_req,4) as kode', FALSE);
		$this->db->order_by('id_req','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('tbl_req');      
		if($query->num_rows() <> 0){      

			$data = $query->row();      
			$kode = intval($data->kode) + 1;    
		}
		else {      

			$kode = 1;    
		}

		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT); 

		  $kodejadi = "PPDB-2020-".$kodemax;    // hasilnya PPDB-2020-0001 dst.
		  return $kodejadi;  
		}

		function get_data_nisn($nisn){

			$hsl=$this->db->query("SELECT * FROM tbl_req WHERE nisn='$nisn'");
			if($hsl->num_rows()>0){
				foreach ($hsl->result() as $data) {
					$hasil=array(
						'nisn' => $data->nisn,
						'name' => $data->name,
						'email' => $data->email,
						'no_reg' => $data->no_reg,
					);
				}
			}
			return $hasil;
		}

		function getsiswa(){
			$this->db->select('*');
			$this->db->from('tbl_req');
			$this->db->order_by('nisn','ASC');

			return $query=$this->db->get();
			if ($query->num_rows()>0) {
				return $query->result();
			}
		}

		public function getdelete_siswa($key)
    {
        $this->db->where('id_req',$key)
        ->delete('tbl_req');
    }

	}