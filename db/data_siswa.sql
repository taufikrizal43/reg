-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2020 at 12:06 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data_siswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(3) NOT NULL,
  `no_reg` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `no_reg`, `keterangan`) VALUES
(1, 'PPDB-2020-0001 sd PPDB-2020-00020', 'Pengambilan Pin Dimulai tgl 01-05-2020 sd 02-05-20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_req`
--

CREATE TABLE `tbl_req` (
  `id_req` int(3) NOT NULL,
  `nisn` int(12) NOT NULL,
  `name` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `active` enum('0','1') DEFAULT NULL,
  `no_reg` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_req`
--

INSERT INTO `tbl_req` (`id_req`, `nisn`, `name`, `email`, `hp`, `active`, `no_reg`) VALUES
(1, 1234567891, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', NULL, 'PPDB-2020-0001'),
(2, 1, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0002'),
(3, 111, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0003'),
(4, 123456, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0004'),
(5, 1112, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0005'),
(6, 1234567, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0006'),
(7, 1, '', '', '', '', 'PPDB-2020-0007'),
(8, 9, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0008'),
(9, 56, 'taufik syaiful rizal', 'taufikrizal43@gmail.com', '085649251172', '', 'PPDB-2020-0009');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `tbl_req`
--
ALTER TABLE `tbl_req`
  ADD PRIMARY KEY (`id_req`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_req`
--
ALTER TABLE `tbl_req`
  MODIFY `id_req` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
